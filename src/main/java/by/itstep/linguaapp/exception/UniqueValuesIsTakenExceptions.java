package by.itstep.linguaapp.exception;

public class UniqueValuesIsTakenExceptions extends RuntimeException {

    public UniqueValuesIsTakenExceptions (String message) {
        super(message);
    }
}
