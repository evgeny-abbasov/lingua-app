package by.itstep.linguaapp.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class UserEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;
    
    @Column(name = "country")
    private String country;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserRole role;
    
    @Column(name = "phone")
    private String phone;

    @Column(name = "blocked")
    private Boolean blocked;

    @Column(name = "last_answer_date")
    private Instant lastAnswerDate;

    @Column(name = "last_email_date")
    private Instant lastEmailDate;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @ManyToMany(mappedBy = "usersWhoCompleted")
    private List<QuestionEntity> completedQuestions = new ArrayList<>();

}
