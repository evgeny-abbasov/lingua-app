package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer> {

    CategoryEntity findByName(String name);
}
