package by.itstep.linguaapp.service;

public interface MailService {

    void sendEmail(String email, String message);

    void sendScheduledNotifications();
}
