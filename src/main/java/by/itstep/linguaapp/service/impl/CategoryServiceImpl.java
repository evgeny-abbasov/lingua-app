package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValuesIsTakenExceptions;
import by.itstep.linguaapp.mapper.CategoryMapper;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryMapper categoryMapper;
    private final CategoryRepository categoryRepository;

    @Override
    @Transactional
    public CategoryFullDto create(CategoryCreateDto createDto) {
        CategoryEntity entityToSave = categoryMapper.map(createDto);

        CategoryEntity entityWithSameName = categoryRepository.findByName(entityToSave.getName());

        if (entityWithSameName != null) {
            throw new UniqueValuesIsTakenExceptions("Name is taken!");
        }
        CategoryEntity savedEntity = categoryRepository.save(entityToSave);
        CategoryFullDto categoryDto = categoryMapper.map(savedEntity);

        log.info("Category was successfully created");
        return categoryDto;
    }

    @Override
    @Transactional
    public CategoryFullDto update(CategoryUpdateDto dto) {
        CategoryEntity categoryToUpdate = categoryRepository.findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException("CategoryEntity was not found by id " + dto.getId()));

        categoryToUpdate.setName(dto.getName());

        CategoryEntity updateCategory = categoryRepository.save(categoryToUpdate);
        CategoryFullDto categoryDto = categoryMapper.map(updateCategory);

        log.info("Category was successfully updated");
        return categoryDto;
    }

    @Override
    @Transactional
    public CategoryFullDto findById(int id) {
        CategoryEntity foundCategory = categoryRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("CategoryEntity was not found by id " + id));

        CategoryFullDto categoryDto = categoryMapper.map(foundCategory);
        log.info("Category was successfully found");
        return categoryDto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryFullDto> findAll() {
        List<CategoryEntity> foundCategories = categoryRepository.findAll();
        List<CategoryFullDto> dtos = categoryMapper.map(foundCategories);
        log.info(dtos.size() + " Categories were found");
        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        CategoryEntity categoryToDelete = categoryRepository.findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("CategoryEntity was not found by id " + id));

        categoryToDelete.setDeletedAt(Instant.now());
        categoryRepository.save(categoryToDelete);
        log.info("Category was successfully deleted");

    }
}
