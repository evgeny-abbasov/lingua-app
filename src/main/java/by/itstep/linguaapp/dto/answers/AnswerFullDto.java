package by.itstep.linguaapp.dto.answers;

import lombok.Data;

@Data
public class AnswerFullDto {

    private Integer id;
    private String body;
    private Boolean correct;
}
