package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping("/categories")
    public CategoryFullDto create(@Valid @RequestBody CategoryCreateDto dto) {
        return categoryService.create(dto);
    }

    @PutMapping("/categories")
    public CategoryFullDto update(@Valid @RequestBody CategoryUpdateDto dto) {
        return categoryService.update(dto);
    }

    @GetMapping("/categories/{id}")
    public CategoryFullDto findById(@PathVariable Integer id) {
        return categoryService.findById(id);
    }

    @GetMapping("/categories")
    public List<CategoryFullDto> findAll() {
        return categoryService.findAll();
    }

    @DeleteMapping("/categories/{id}")
    public void delete(Integer id) {
        categoryService.delete(id);
    }
}
