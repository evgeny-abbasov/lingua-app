package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto dto) {
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@Valid @RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public Page<UserFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return userService.findAll(page, size);
    }

    @DeleteMapping("/users/{id}")
    public void delete(Integer id) {
        userService.delete(id);
    }

    @PutMapping("/users/password")
    public void changePassword(@Valid @RequestBody ChangeUserPasswordDto dto) {
        userService.changePassword(dto);
    }

    @PutMapping("/users/role")
    public void changeRole(@Valid @RequestBody ChangeUserRoleDto dto) {
        userService.changeRole(dto);
    }

    @PutMapping("/users/{id}/block")
    public void block(Integer id, @RequestHeader("Authorization") String authorization) throws Exception {
        userService.block(id);
    }
}
