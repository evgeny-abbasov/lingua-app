package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class QuestionController {

    private final QuestionService questionService;

    @PostMapping("/questions")
    public QuestionFullDto create(@Valid @RequestBody QuestionCreateDto dto) {
        return questionService.create(dto);
    }

    @PutMapping("/questions")
    public QuestionFullDto update(@Valid @RequestBody QuestionUpdateDto dto) {
        return questionService.update(dto);
    }

    @GetMapping("/questions/{id}")
    public QuestionFullDto findById(@PathVariable Integer id) {
        return questionService.findById(id);
    }

    @GetMapping("/questions")
    public List<QuestionShortDto> findAll() {
        return questionService.findAll();
    }

    @DeleteMapping("/questions/{id}")
    public void delete(Integer id) {
        questionService.delete(id);
    }

    @PostMapping("/questions/{questionId}/answers/{answerId}/check")
    public boolean checkAnswer(@PathVariable Integer questionId,@PathVariable Integer answerId) {
        return questionService.checkAnswer(questionId, answerId);
    }

    @GetMapping("/categories/{categoryId}/questions/random")
    public QuestionFullDto getRandomQuestion(@PathVariable Integer categoryId) {
        return questionService.getRandomQuestion(categoryId);
    }
}
